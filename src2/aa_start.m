rosinit;

figure(1);
hold off;
clf;

global pubCmdVel pubPose1 pubObstacle pubOffset pubAlpha pubInsideRow;
base = robotics.ros.Node("/base");
pubCmdVel = robotics.ros.Publisher(base, '/cmd_vel', 'geometry_msgs/Twist');
pubPose1 = robotics.ros.Publisher(base, "/pose1", "geometry_msgs/Pose2D");
pubObstacle = robotics.ros.Publisher(base, "/obstacle", "geometry_msgs/Point");
pubOffset = robotics.ros.Publisher(base, "offset", "std_msgs/Float64");
pubAlpha = robotics.ros.Publisher(base, "alpha", "std_msgs/Float64");
pubInsideRow = robotics.ros.Publisher(base, "inside_row", "std_msgs/Bool");
nav_sub = robotics.ros.Subscriber(base, "base_scan", @callback);