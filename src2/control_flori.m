function control_flori(d_alpha_in, offset) 
    persistent temp_alpha;

    %Einlesen der Winkelabweichung
    if isempty(temp_alpha)
        temp_alpha = d_alpha_in;
    end

    %Ausgangsgröße des Reglers um die Winkelabweichung zu veringern
    d_alpha = PID_alpha(d_alpha_in); %Wertebereich von -PI()/2 bis PI()/2

    %Hyperbolische, prozentuale Gewichtung der Ausgangsgröße des Reglers
    X = 1 / (abs(d_alpha * 10));

    %Begrenzung der gewichteten Ausgangsgröße auf 1
    if(X > 1)
        X = 1;
    end
    
    %Falls Alpha NaN wird das vorherige Alpha benutzt
    if isnan(d_alpha)
        d_alpha = temp_alpha;
        clear err_a err_prev_a I_prev_a
    end
    
    %Falls Offset NaN wird dieser auf 0 gesetzt
    if isnan(offset)
        offset = 0;
        clear err err_prev I_prev 
    end
    
    temp_alpha = d_alpha;

    %Kombination aus Winkelabweichung und Positionsabweichung ergibt den 
    %Wert für die Positionsänderung  
    moveFlori(X*(1-(abs(offset))), d_alpha);

end

function [u_out] = PID_alpha(err_input_a)
    Kp_a = .7;      %Konstante für den P-Anteil
    Ki_a = .02;     %Konstante für den I-Anteil
    Kd_a = .02;     %Konstante für den D-Anteil
    d_t_a = 0.1;    %Diskrete Zeitschritte der Simulation 100 Millisekunden
    
    %Differenz für die Differenzengleichung
    persistent err_prev_a I %Definition des vorherigen Fehlers
    if isempty(err_prev_a) || isnan(err_prev_a)
        err_prev_a = err_input_a; %Beschreiben des vorherigen Fehlers
    end

    %Integralanteil des Reglers
    if isempty(I) || isnan(I)
        I = 0;
    end

    P = Kp_a*err_input_a;
    I = Ki_a*(err_input_a*d_t_a) + I;
    D = Kd_a*(err_input_a-err_prev_a)/d_t_a;
    u_out = P + I + D; %Ausgangsgröße des Reglers

    %Begrenzung des Wertebereichs des Integralanteils
    if(I > .03)
        I = .03;
    elseif(I < -.03)
        I = -.03;
    end

    %Fehlermeldung falls Ausgangsgröße NaN ist
    if isnan(u_out)
        disp('a is nan')
    end

    %Für den nächsten Aufruf vorherigen Fehlerwert durch aktuellen Fehlerwert ersetzen
    err_prev_a = err_input_a;
end
