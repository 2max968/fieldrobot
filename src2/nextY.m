function [u, o] = nextY(cartesians, maxX)
    u = min(cartesians(:,2));
    o = max(cartesians(:,2));
    n = length(cartesians);   %Nach getROI im Programmablauf, deshalb Anzahl aller gefunden Pflanzen
    for i = 1:n
        if(cartesians(i,1) < maxX) %Falls die erkannte Pflanze nicht weiter weg ist als maxX (0,8m)
            v = cartesians(i,2); %Schreiben der Y-Koordinate dieser nahen Pflanze in v
            
            %Im ersten Durchgang: Falls die Y-Koordinate der nahen Pflanze kleiner ist als der höchste Wert aller
            %anderen Y-Koordinaten von Pflanzen in der ROI, wird die Y-Koordinate die neue Obergrenze
            %Ab dem zweiten Durchgang: Die letzte berechnete Obergrenze
            if(v > 0 && v < o)    
                o = v; %obere Pflanzenreihe
            
            %Analog zur Obergrenze
            elseif(v < 0 && v > u)
                u = v; %untere Pflanzenreihe
            end
        end
    end
end
