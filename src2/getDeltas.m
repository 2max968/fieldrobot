function [deltas, alphas, count] = getDeltas(inp)
    tolerance = [0.40, 0.55];
    n = length(inp);
    % Wenn nicht genügend Punkte vorhanden sind, wird die Funktion
    % abgebrochen
    if(n <= 2)
        deltas = nan;
        alphas = nan;
        count = 0;
    else
        deltas=nan(size(inp));
        alphas=nan(1, length(inp));
        count = 0;
        for i = 1:n
            cx = inp(i,1);
            cy = inp(i,2);
            % Den aktuellen Punkt mit allen anderen Punkten vergleichen
            for j = 1:n
                px = inp(j,1);
                py = inp(j,2);
                % Nur weiterarbeiten, wenn der zweite Punkt rechts des
                % linken Punkts sitzt
                if(px > cx)
                    dx = px-cx;
                    dy = py-cy;
                    d = sqrt(dx*dx+dy*dy);
                    % Vergleichen ob der Abstand der beiden Punkte zwischen
                    % 0,4 und 0,55 Meter liegt
                    if(d > tolerance(1) && d < tolerance(2))
                        % Den Verbindungsvektor zwischen den beiden Punkten
                        % abspeichern
                        deltas(i,1) = dx;
                        deltas(i,2) = dy;
                        % Den Winkel zwischen den beiden Punkten berechnen
                        % und abspeichern
                        alphas(i) = atan2(dy,dx);
                        % Den Zähler der gefundenen Verbindungen um eins
                        % erhöhen
                        count = count + 1;
                    end
                end
            end
        end
    end
end