function [eof, d, alpha] = calcPathParameters(msg, extendROI)
    % Bereich festlegen, der betrachtet werden soll, da zu viele
    % Punkte nur stören.
    box = [0,1.2,-2.5,2.5];
    if(extendROI)
        box(2) = 2;
    end
    % Abstandswerte des Laserscanners in Kartesische Koordinaten umrechnen
    cartesians = readCartesian(msg);
    % Punkte außerhalb des zu betrachtenten Bereichs löschen
    cartesians = getROI(cartesians, box(1), box(2), box(3), box(4));
    % Punkte, die näher als 0,1m zusammenliegen, werden zu einem Punkt
    % zusammengefasst, damit wird jede Pflanze nur als ein Punkt erkannt
    cartesians = collapsePoints(cartesians, .1);
    % Pflanzen so miteinander verbinden, dass Spuren gefunden werden können
    [deltas, alphas, nConnections] = getDeltas(cartesians);
    if (nConnections > 0)
        % Mit der Hilfe eines Histogramms wird die Verdrehung des Roboters
        % zu den Pflanzenreihen bestimmt
        alpha = alphaHist(alphas, 5 - cartesians(:,1));
        % Die Punkte werden um die Verdrehung des Roboters gedreht, damit
        % die Pflanzen einer Reihe einen gemeinsamen Y-Wert haben
        cartesiansRot = rotatePoints(cartesians, -alpha);
        % Den Y-Wert der nächsten beiden Pflanzenreihen bestimmen
        [lineU, lineO] = nextY(cartesiansRot, 0.8);
        % Die Breite der aktuellen Spur berechnen
        lineD = lineO - lineU;
        if(lineD < 0.8)
            % Wenn die Spur schmal ist, soll sich der Roboter in der Mitte
            % der Spur halten.
            d = (lineO + lineU) / 2;
        elseif(lineO < -lineU)
            % Wenn die Spur zu breit ist, hält der Roboter sich an die
            % näheren Pflanzenreihe.
            d = lineO - 0.325;
        else
            d = lineU + 0.325;
        end
    
        % Informationen plotten
        clf;
        hold on;
        xlim([0 5]);
        ylim([-2.5,2.5]);
        if(length(cartesians) >= 1)
            plot(cartesians(:,1), cartesians(:,2), 'x');
            plot(cartesiansRot(:,1), cartesiansRot(:,2), 'o');
            for i = 1:length(cartesians)
                x1 = cartesians(i,1);
                y1 = cartesians(i,2);
                x2 = x1 + deltas(i, 1);
                y2 = y1 + deltas(i, 2);
                plot([x1 x2], [y1 y2], '-');
            end
            quiver(0, d, cos(alpha), sin(alpha) + d, 0, 'MaxHeadSize', 0.5);
        end
    
        yline(lineO);
        yline(lineU);
    
        eof = 0;
    else
        % Falls nicht genügend Punkte gefunden wurden, wird die Funktion
        % erneut aufgerufen mit einer größeren Region of Interest
        if(~extendROI)
            [eof, d ,alpha] = calcPathParameters(msg, true);
        else
            alpha = nan;
            d = nan;
            eof = 1;
        end
    end
end
