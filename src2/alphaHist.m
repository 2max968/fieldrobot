function angle = alphaHist(alphas, weights)
    
    h = histogram(alphas, 20, 'visible', 'off');
    [~, angle] = max(h.Values);
    angle = h.BinEdges(angle) + h.BinWidth / 2;
end