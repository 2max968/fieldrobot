function res = getROI(inp, x1, x2, y1, y2)
    n = length(inp); %Anzahl der vom Laserscanner erkannten Punkte
    p = 1; %Zählervariable für Punkte die sich in der ROI befinden
    for i = 1:n
        x = inp(i, 1);
        y = inp(i, 2);
        %Falls die erkannten Punkte in der ROI liegen
        %werden sie in 'res' gespeichert
        if(x >= x1 && x <= x2 && y >= y1 && y <= y2)
            res(p,1) = x;
            res(p,2) = y;
            p = p + 1;
        end
    end
    %Falls nur ein Punkt oder weniger gefunden wird
    %ist das Ergebnis NaN
    if(p <= 1)
        res = nan(0,0);
    end
end
