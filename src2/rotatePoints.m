function outp = rotatePoints(inp, alpha)
    n = length(inp);
    mat = [cos(alpha),sin(alpha);-sin(alpha),cos(alpha)];
    outp = nan(size(inp));

    for i = 1:n
        p = inp(i, :);
        p = p * mat;
        outp(i, :) = p';
    end
end