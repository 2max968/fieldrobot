function callback(sub, msg)
    global pubOffset pubAlpha pubInsideRow;

    % Persistente Variablen:
    %      - state: Der aktuelle Zustand des Programms
    %      - stateTime: Die Zeit, die sich das Programm bereits im
    %        aktuellen Zustand befindet
    %        Die Zeit wird bei jedem Aufruf der Callback-Funktion um 0,1
    %        erhöht, da die Simulation 10 Simulationsschritte pro Sekunde
    %        macht
    %      - right: Variable die speichert, ob der Roboter nach rechts oder
    %        links in die nächste Reihe fahren soll
    persistent state stateTime right;

    if(isempty(state))
        state = 0;
    end

    if(isempty(right))
        right = 0;
    end
    
    % Berechnen, ob sich der Roboter noch im Feld befindet und falls ja,
    % die Abweichung des Winkels und der position vom Mittelpunkt berechnen
    [eof, d, alpha] = calcPathParameters(msg, false);
    
    % ROS-Message senden, die aussagt ob sich der Roboter im Feld befindet
    msgInsideRow = rosmessage(pubInsideRow);
    msgInsideRow.Data = state < 2;
    pubInsideRow.send(msgInsideRow);
    
    % Switch-Statement zur Unterscheidung des aktuellen Zustands
    switch(state)
        case 0 % Init
            state = 1;
        case 1 % Fahren
            % Die Abweichungen in den Regler geben
            control_flori(alpha + d * 5, d);

            % Abweichungen in einem ROS-Topic veröffentlichen
            msgOffset = rosmessage(pubOffset);
            msgOffset.Data = d;
            pubOffset.send(msgOffset);
            msgAlpha = rosmessage(pubAlpha);
            msgAlpha.Data = alpha;
            pubAlpha.send(msgAlpha);
            if(eof)
                state = 2;
                stateTime = 0;%tic / 1000000;
            end
        case 2 % Feld Ende
            stateTime = stateTime + .1;
            if(~eof)
                state = 1;
            end
            moveFlori(1,0);
            if(stateTime > 1)
                state = 3;
                stateTime = 0;
                right = ~right;
            end
        case 3 % Wende
            stateTime = stateTime + .1;
            if(stateTime > 3)
                state = 1;
            else
                if(right)
                    moveFlori(0.4, -1);
                else
                    moveFlori(0.4, 1);
                end
            end  
    end
end