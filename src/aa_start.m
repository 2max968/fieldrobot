rosinit;

figure(1);
hold off;
clf;

global pubCmdVel pubPose1 pubObstacle smsg;
base = robotics.ros.Node("/base");
pubCmdVel = robotics.ros.Publisher(base, '/cmd_vel', 'geometry_msgs/Twist');
pubPose1 = robotics.ros.Publisher(base, "/pose1", "geometry_msgs/Pose2D");
pubObstacle = robotics.ros.Publisher(base, "/obstacle", "geometry_msgs/Point");
nav_sub = robotics.ros.Subscriber(base, "base_scan", @callback);