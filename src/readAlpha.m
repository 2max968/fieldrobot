function angle = readAlpha(cartesian)
    k = 3;
    n = length(cartesian) - k;
    alphas = nan(1, n);
    for i=1:n
        y1 = cartesian(i,2);
        y2 = cartesian(i+k,2);
        x1 = cartesian(i,1);
        x2 = cartesian(i+k,1);
        alpha = atan2(y2-y1,x2-x1);
        alphas(i) = mod(alpha, pi);
    end

    h = histogram(alphas, 80, 'visible', 'off');
    [~, angle] = max(h.Values);
    angle = h.BinEdges(angle) + h.BinWidth / 2;
end