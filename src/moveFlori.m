function moveFlori(dist, alpha)
    global pubCmdVel;

    msg = rosmessage(pubCmdVel);
    msg.Linear.X = dist;
    msg.Linear.Y = 0;
    msg.Linear.Z = 0;
    msg.Angular.X = 0;
    msg.Angular.Y = 0;
    msg.Angular.Z = alpha;
    pubCmdVel.send(msg);
end