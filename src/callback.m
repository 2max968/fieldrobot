function callback(sub, msg)
    global pubCmdVel pubPose1 pubObstacle smsg;
    persistent state stateTime;

    if(isempty(state))
        state = 0;
    end

    smsg = msg;

    [eof, d, alpha] = calcPathParameters(msg);

    switch(state)
        case 0 % Init
            state = 1;
        case 1 % Fahren
            if(eof)
                state = 2;
                stateTime = tic / 1000000;
            end
        case 2 % Feld Ende
            moveFlori(1,0);
            time = tic / 1000000;
            moveFlori(1,0);
            if(time - stateTime > 0.5)
                state = 3;
            end
        case 3 % Wende
            if(~eof)
                state = 1;
            else
                moveFlori(0.4,1);
            end
        case 4 % Rotation
            
    end
end