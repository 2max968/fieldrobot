function res = collapsePoints(inp, threshold)
    n = length(inp);
    gi = 1;
    groups = zeros(1, n);
    thresholdSq = threshold * threshold;
    for i = 1:n
        cg = groups(i);
        if(cg == 0)
            cg = gi;
            gi = gi + 1;
        end
        groups(i) = cg;
        for j = (i+1):n
            dx = inp(i,1) - inp(j,1);
            dy = inp(i,2) - inp(j,2);
            distSq = dx*dx + dy*dy;
            if(distSq < thresholdSq)
                groups(j) = cg;
            end
        end
    end
    
    p = 1;
    res = nan(gi-1,2);
    for i = 1:(gi-1)
        xsum = 0;
        ysum = 0;
        num = 0;
        for j=1:n
            if(groups(j) == i)
                xsum = xsum + inp(j,1);
                ysum = ysum + inp(j,2);
                num = num + 1;
            end
        end
        res(p,1) = xsum / num;
        res(p,2) = ysum / num;
        p = p + 1;
    end
    if(p <= 1)
        res = nan(0,0);
    end
end