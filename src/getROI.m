function res = getROI(inp, x1, x2, y1, y2)
    n = length(inp);
    p = 1;
    for i = 1:n
        x = inp(i, 1);
        y = inp(i, 2);
        if(x >= x1 && x <= x2 && y >= y1 && y <= y2)
            res(p,1) = x;
            res(p,2) = y;
            p = p + 1;
        end
    end
    if(p <= 1)
        res = nan(0,0);
    end
end