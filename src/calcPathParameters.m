function [eof, d, alpha] = calcPathParameters(msg)
    box = [0,5,-.8,.8]; % xmin, xmax, ymin, ymax
    
    cartesians = readCartesian(msg);
    cartesians = getROI(cartesians, box(1), box(2), box(3), box(4));
    cartesians = collapsePoints(cartesians, .1);
    pairs = findPairs(cartesians, .7,.8,-pi/8,pi/8);
    pairnum = size(pairs,1);
    result = 0;
    if(pairnum > 1)
        pairs = sortPairs(pairs);
        mids = [(pairs(1:2,1) + pairs(1:2,3)) / 2, (pairs(1:2,2) + pairs(1:2,4)) / 2];
        pathM = (mids(2,2)-mids(1,2))/(mids(2,1)-mids(1,1));
        pathY = mids(2,2) - mids(2,1)*pathM;
        result = 1;
    end
    %alpha = readAlpha(cartesians);

    clf;
    hold on;
    xlim([0 5]);
    ylim([-2.5,2.5]);
    if(length(cartesians) >= 1)
        plot(cartesians(:,1), cartesians(:,2), 'x');
    end
    for i = 1:pairnum
        x1 = pairs(i,1);
        y1 = pairs(i,2);
        x2 = pairs(i,3);
        y2 = pairs(i,4);
        if(i <= 2)
            plot([x1,x2], [y1,y2], '--');
        else
            plot([x1,x2], [y1,y2], '-.');
        end
    end
    if(result == 1)
        x1 = 0;
        x2 = 5;
        y1 = pathY;
        y2 = pathY + x2 * pathM;
        plot([x1 x2], [y1, y2], '-');
    end
    yline(box(3));
    yline(box(4));

    eof = ~result;
    if(result)
        d = pathY;
        alpha = atan2(mids(2,2)-mids(1,2), mids(2,1)-mids(1,1));
    else
        d = nan(1);
        alpha = nan(1);
    end
end