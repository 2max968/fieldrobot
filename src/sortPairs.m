function sorted = sortPairs(inp)
    mids = [(inp(:,1) + inp(:,3)) / 2, (inp(:,2) + inp(:,4)) / 2];
    dists = sqrt(mids(:,1).*mids(:,1)+mids(:,2).*mids(:,2));
    [~,I] = sort(dists);
    sorted = [inp(I,1), inp(I,2), inp(I,3), inp(I,4)];
end