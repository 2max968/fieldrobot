% Pairs:
% x1 | y1 | x2 | y2

function res = findPairs(inp, d1, d2, a1, a2)
    n = length(inp);
    if(n <= 4)
        res = nan(0,0);
        return;
    end
    p = 1;
    for i = 1:n
        p2 = 1;
        poi = nan(0,0);
        for j = 1:n
            dx = inp(i,1) - inp(j,1);
            dy = inp(i,2) - inp(j,2);
            dist = sqrt(dx*dx + dy*dy);
            alpha = atan2(dy, dx) + pi/2;
            if(dist >= d1 && dist <= d2 && alpha >= a1 && alpha <= a2)
                poi(p2,1) = inp(j,1);
                poi(p2,2) = inp(j,2);
                poi(p2,3) = dist;
                p2 = p2 + 1;
            end
        end
        if(p2 > 1)
            [~,bestInd] = min(poi(:,3) - (d1+d2) / 2);
            res(p, 1) = inp(i,1);
            res(p, 2) = inp(i,2);
            res(p, 3) = poi(bestInd, 1);
            res(p, 4) = poi(bestInd, 2);
            p = p + 1;
        end
    end
    if(p <= 1)
        res = nan(0,0);
    end
end